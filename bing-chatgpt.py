from time import sleep
import requests
import webbrowser
from bs4 import BeautifulSoup
from sys import platform

# Add try except for requests
try:
    # Get the HTML content of the news section
    url = 'https://www.bbc.com/news'
    response = requests.get(url)
    html_content = response.content
    # Extract the headlines or titles using BeautifulSoup
    soup = BeautifulSoup(html_content, 'html.parser')
    headline_tags = soup.find_all('h3', class_='gs-c-promo-heading__title')
    headlines = [tag.get_text().strip() for tag in headline_tags]

    # Use the headlines to populate the search_kw variable
    search_kw = [headline.replace(' ', '+') for headline in headlines]

    # Detect OS
    if platform == 'darwin':
        edge_path = '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge'
    elif platform == 'win32':
        edge_path = 'C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe'
        #edge_path =  'C:\Program Files\Mozilla Firefox\\firefox.exe'
    webbrowser.register('edge', None, webbrowser.BackgroundBrowser(edge_path))
    #webbrowser.register('firefox', None, webbrowser.BackgroundBrowser(edge_path))
    search1 = 'https://www.bing.com/search?pglt=161&q='
    search2 = '&cvid=5acae788eb5541d1a4597e87a98e698a&gs_lcrp=EgZjaHJvbWUqBggAEAAYQDIGCAAQABhAMgYIARAuGEAyBggCEAAYQDIGCAMQABhAMgYIBBAAGEAyBggFEAAYQDIGCAYQABhAMgYIBxAAGEAyBggIEAAYQNIBCDI4OThqMGoxqAIAsAIA&FORM=ANSPA1&PC=U531'
    # 'i' second delay between each search
    i = 1
    for kw in search_kw[1:60]:        
        webbrowser.get('edge').open(search1+kw+search2)
        #webbrowser.get('firefox').open(search+kw)
        sleep(i)
        i *= 0.2
except requests.RequestException as e:
    print(f"Exiting. HTTP request failed: {e}")